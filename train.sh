set -e

TOOLS=~/hipCaffe/build/tools
LOG=log-`date +%Y-%m-%d-%H-%M-%S`.log

$TOOLS/caffe train -gpu 0\
             --solver=solver.prototxt $@ | tee $LOG
