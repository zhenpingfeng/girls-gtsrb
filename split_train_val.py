import os
from PIL import Image
from shutil import copyfile
import pandas as pd

TRAIN_PATH = "./GTSRB/Final_Training/Images/"


if __name__ == '__main__':
    try:
        os.mkdir('TrainFile')
        os.mkdir('ValidFile')
    except OSError:
        pass

    
    for f in range(0, 43):
        folder = format(f, '05d')
        csv_path = TRAIN_PATH + folder + '/GT-' + folder + '.csv'
        csv = pd.read_csv(csv_path, sep=';')
        ppm_names = csv['Filename']

        for i in range(0, len(ppm_names)):
            if i % 3 == 0:
                copyfile(TRAIN_PATH + folder + '/' + csv['Filename'][i],
                         './ValidFile/' + format(f, '02d') + '_' +
                         csv['Filename'][i])
            else:
                copyfile(TRAIN_PATH + folder + '/' + csv['Filename'][i],
                         './TrainFile/' + format(f, '02d') + '_' +
                         csv['Filename'][i])
