import sys
import caffe
import cv2
import numpy as np
import pandas as pd


TEST_DATA = "./GTSRB/Final_Test/Images/"

#size of ppm images
IMAGE_WIDTH = 64
IMAGE_HEIGHT = 64

def transform_img(img, img_width=IMAGE_WIDTH, img_height=IMAGE_HEIGHT):

    # Histogram Equalization
    img[:, :, 0] = cv2.equalizeHist(img[:, :, 0])
    img[:, :, 1] = cv2.equalizeHist(img[:, :, 1])
    img[:, :, 2] = cv2.equalizeHist(img[:, :, 2])

    # Image Resizing
    img = cv2.resize(img, (img_width, img_height), interpolation = cv2.INTER_LINEAR)

    return img


if __name__ == "__main__":
    model_path = sys.argv[1]
    csv_path = TEST_DATA + "/GT-final_test.test.csv"
    csv = pd.read_csv(csv_path, sep=';')
    ppm_names = csv['Filename']
    out_txt = "sub.txt"

    caffe.set_mode_gpu()
    caffe.set_device(0);
    net = caffe.Net("sub_densenet.prototxt", model_path, caffe.TEST)
    transformer = caffe.io.Transformer({'Data1': net.blobs['Data1'].data.shape})
    mean_data = np.load('meanTrain.npy').mean(1).mean(1)
    print(mean_data)

    with open(out_txt, 'w') as f:
        for i in range(0, 12630):
            img = cv2.imread(TEST_DATA + ppm_names[i], cv2.IMREAD_COLOR)
            img = transform_img(img, IMAGE_WIDTH, IMAGE_HEIGHT)
            #img = img[8:120, 8:120]
            img = img - mean_data
            img = img[np.newaxis,...].transpose(0,3,1,2)

            net.blobs['Data1'].data[...] = img

            res = net.forward()
            prob = res['loss']
            idx = np.argmax(prob)
            print(ppm_names[i] + ":" +str(idx))
            f.writelines(ppm_names[i] + "; " + str(idx) + "\n")
