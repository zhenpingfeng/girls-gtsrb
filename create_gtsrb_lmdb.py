import os
import glob
import random
import numpy as np

import cv2

import caffe
from caffe.proto import caffe_pb2
import lmdb

#size of ppm images
IMAGE_WIDTH = 64
IMAGE_HEIGHT = 64

TRAIN_DATA = './TrainFile'
VALID_DATA = './ValidFile'

TRAIN_LMDB = 'gtsrb_train_lmdb'
VALID_LMDB = 'gtsrb_valid_lmdb'


def transform_img(img, img_width=IMAGE_WIDTH, img_height=IMAGE_HEIGHT):

    # Histogram Equalization
    img[:, :, 0] = cv2.equalizeHist(img[:, :, 0])
    img[:, :, 1] = cv2.equalizeHist(img[:, :, 1])
    img[:, :, 2] = cv2.equalizeHist(img[:, :, 2])

    # Image Resizing
    img = cv2.resize(img, (img_width, img_height), interpolation = cv2.INTER_LINEAR)

    return img


def make_datum(img, label):

    #image is numpy.ndarray format. BGR instead of RGB
    return caffe_pb2.Datum(
        channels=3,
        width=IMAGE_WIDTH,
        height=IMAGE_HEIGHT,
        label=label,
        data=np.rollaxis(img, 2).tobytes())


if __name__ == '__main__':
    try:
        os.remove(TRAIN_LMDB)
        os.remove(VALIDATION_LMDB)
    except OSError:
        pass

    train_data = [img for img in glob.glob(TRAIN_DATA + "/*.ppm")]
    valid_data = [img for img in glob.glob(VALID_DATA + "/*.ppm")]

    random.shuffle(train_data)


    print("Creating " + TRAIN_LMDB)

    in_db = lmdb.open(TRAIN_LMDB, map_size=int(1e12))
    with in_db.begin(write=True) as in_txn:
        for in_idx, img_path in enumerate(train_data):
            img = cv2.imread(img_path, cv2.IMREAD_COLOR)
            img = transform_img(img, img_width=IMAGE_WIDTH, img_height=IMAGE_HEIGHT)
            label = int(img_path.split('/')[-1].split('_')[0])
            datum = make_datum(img, label)
            in_txn.put('{:0>5d}'.format(in_idx), datum.SerializeToString())
            print('{:0>5d}'.format(in_idx) + ':' + img_path)
    in_db.close()


    print("Creating " + VALID_LMDB)

    in_db = lmdb.open(VALID_LMDB, map_size=int(1e12))
    with in_db.begin(write=True) as in_txn:
        for in_idx, img_path in enumerate(valid_data):
            # if in_idx % 6 != 0:
            #     continue
            img = cv2.imread(img_path, cv2.IMREAD_COLOR)
            img = transform_img(img, img_width=IMAGE_WIDTH, img_height=IMAGE_HEIGHT)
            label = int(img_path.split('/')[-1].split('_')[0])
            datum = make_datum(img, label)
            in_txn.put('{:0>5d}'.format(in_idx), datum.SerializeToString())
            print('{:0>5d}'.format(in_idx) + ':' + img_path)
    in_db.close()

    print("Finished processing all images.")
